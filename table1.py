import sys
from os.path import join, basename
from time import clock
from cylp.cy import CyClpSimplex
from cylp.py.pivots import PositiveEdgePivot
from cylp.py.pivots import DantzigPivot

pds_dir = '../pds'

def solve(filename, method):
    '''
    Read the problem in ``filename`` and solve it
    using a specific pivot ``method``.
    (d for Dantzig, p for positive edge)
    Return the number of iterations and the run time.
    '''
    s = CyClpSimplex()
    s.readMps(filename)

    s = s.preSolve(feasibilityTolerance=10 ** -8)

    if method == 'd':
        pivot = DantzigPivot(s)
    elif method == 'p':
        pivot = PositiveEdgePivot(s)
    else:
        print 'Unkown pivot %s.' % method
        sys.exit(1)

    s.setPivotMethod(pivot)

    start = clock()
    s.primal()
    t = clock() - start
    #print 'Problem solved in %g seconds.' % (clock() - start)
    #print 'Iterations: %d' % s.iteration
    return s.iteration, t

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'Please provide the path to PDS problems'
        sys.exit(1)

    with open('table1.dat', 'w') as f:
        f.write('%-13s %10s %10s   %s\n' %
                ('Problem', 'i_d',  'i_p', 'speedup'))

    problems = [join(pds_dir, 'pds-%s.mps' % s) for s in
                ('02', '06', '10','20', '30', '40')]
    problems = problems[-1:]
    for problem in problems:
        i_p, t_p = solve(problem, 'p')
        i_d, t_d = solve(problem, 'd')
        print i_d, i_p, t_d/t_p
        with open('table1.dat', 'a') as f:
            f.write('%-13s %10d %10d      %2.1f\n' %
                    (basename(problem), i_d, i_p, t_d/t_p))

